API: The API is made using Python and [fastAPI](https://fastapi.tiangolo.com/)
Frontend: The Frontend is made using React + TypeScript + Vite

## Getting Started

### API:

1 - First, go to the API folder :

```bash
cd herodot-api
```

2 - Create a Python Environment :

```bash
python -m venv .venv
```

3 - Activate .venv Environment :

```python
source .venv/bin/activate
```

4 - Install dependencies :

```python
pip install -r requirements.txt
```

5 - Create .env.local file at the root of herodot-api,

```dosini
# .env.local
API_VERSION=1.0.0
APP_NAME=herodot-technical-test
DEBUG_MODE=true
```

6 - Then, run the API development server:

```bash
uvicorn main:app --reload
```

**Doc: Open [http://localhost:8000/docs](http://localhost:8000) with your browser to see the API documentation.**

> :warning: **Warning:** In main.py file, check if the frontend port used is the same as the one configured in the main.py file. You might encounter a **Bad Request** otherwise

### Frontend:

1 - First, go to the API folder :

```bash
cd herodot-front
```

2 - Create .env file at the root of herodot-front :

```dosini
# .env
VITE_API_URL=http://127.0.0.1:8000
```

3 - Install dependencies :

```bash
npm install
```

4 - Then, run the API development server:

```bash
npm run dev
```

**[frontend url](http://localhost:5173/)**
