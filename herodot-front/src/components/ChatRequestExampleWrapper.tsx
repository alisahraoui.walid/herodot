import { Flex, Typography, } from 'antd';
import React from 'react';
import { OpenaiMessageDTO } from '../dto/OpenaiMessageDTO.ts';
import useFetch from '../hooks/useFetch.ts';
import { CustomButton } from './CustomButton.tsx';

const textExamples: string[] = [ 'Skilled worker', 'Self Employment', 'Lorem' ];
const { Text } = Typography;
const url = import.meta.env.VITE_API_URL;

const RequestExampleWrapperTitle = () => {
  return (
    <Text style={{
      color: '#7fbdb2',
      fontSize: '1.5em',
      fontWeight: 'bold',
    }}>
      How can i help you ?
    </Text>
  )
}

const RequestExampleWrapper = ({setMessages, messages}: {
  setMessages: React.Dispatch<React.SetStateAction<OpenaiMessageDTO[]>>
  messages: OpenaiMessageDTO[]
}) => {
  const { fetchData } = useFetch<OpenaiMessageDTO>(`${url}/chatbot`);

  return (
    <Flex vertical={true} gap={20} justify={'space-between'} style={{height: '100%'}}>
      <Flex vertical={true} gap={10} >
        <RequestExampleWrapperTitle />
        <Flex gap={'small'} wrap={true}>
          { textExamples.map(textExample => {
            const click = async (textExample: string) => {
              const openaiMessageDTO: OpenaiMessageDTO = {
                sender: 'user',
                message: textExample,
                conversationId: '1'
              }

              const requestInit: RequestInit = {
                method: 'POST',
                body: JSON.stringify(openaiMessageDTO)
              }

              const result = await fetchData(requestInit);
              if (result) {
                setMessages([...messages, openaiMessageDTO, result])
              }
            };

            return <CustomButton text={textExample} onClick={() => click(textExample)} />
          })}
        </Flex>
      </Flex>
    </Flex>
  )
}

export {RequestExampleWrapper}
