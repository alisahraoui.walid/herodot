import { Flex, Input } from 'antd';
import React, { useState } from 'react';
import { OpenaiMessageDTO } from '../dto/OpenaiMessageDTO.ts';
import useFetch from '../hooks/useFetch.ts';
import { OpenAiIcon } from '../icons/openai.tsx';
import { CustomButton } from './CustomButton.tsx';

const url = import.meta.env.VITE_API_URL;

const TextBox = ({ setMessages, messages }: {
  setMessages: React.Dispatch<React.SetStateAction<OpenaiMessageDTO[]>>
  messages: OpenaiMessageDTO[]
}) => {
  const style: React.CSSProperties = {
    borderColor: '#fafafa',
    borderWidth: 1,
    borderStyle: 'solid',
    padding: 8,
    borderRadius: 8,
    position: 'absolute',
    bottom: 20,
  }

  const [question, setQuestion] = useState<string>('')
  const { loading, fetchData } = useFetch<OpenaiMessageDTO>(`${url}/chatbot`);
  const handleClick = async () => {
    if (question === '') {
      return
    }

    const openaiMessageDTO: OpenaiMessageDTO = {
      sender: 'user',
      message: question,
      conversationId: '1'
    }

    const requestInit: RequestInit = {
      method: 'POST',
      body: JSON.stringify(openaiMessageDTO)
    }

    const result = await fetchData(requestInit);
    if (result) {
      setQuestion('')
      setMessages([...messages, openaiMessageDTO, result])
    }
  };

  return (
    <Flex style={style} align={'center'} justify={'space-between'}>
      <>
        <OpenAiIcon width={'25'} height={'25'} fill={'#fafafa'}/>
        <Input color={'#fff'} placeholder="More results with chat GPT..." variant="borderless" onChange={(e) => setQuestion(e.target.value)} value={question}/>
      </>

      <CustomButton text={loading ? '...' : 'send'} onClick={() => handleClick()}/>
    </Flex>
  )
}

export { TextBox }
