import { Button, ConfigProvider } from 'antd';
import React from 'react';

const CustomButton = ({ text, onClick}: {text: string | React.ReactNode, onClick: () => void}) => {
  return (
    <ConfigProvider
      theme={{
        components: {
          Button: {
            defaultHoverColor: '#323232',
            defaultHoverBorderColor: '#fafafa',
            defaultHoverBg: '#fafafa',
            defaultBg: '#323232',
            defaultColor: '#fafafa'
          }
        }
      }}
    >
      <Button onClick={onClick}>
        { text }
      </Button>
    </ConfigProvider>
  )
}

export { CustomButton };
