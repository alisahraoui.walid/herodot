import { Layout } from 'antd';
import React from 'react';

const { Header } = Layout;

const headerStyle: React.CSSProperties = {
	textAlign: 'center',
	color: '#fff',
	height: 64,
	paddingInline: 48,
	lineHeight: '64px',
	backgroundColor: '#323232',
	borderBottom: '1px #fff solid',
};

const Navbar = () => {
	return (
		<Header style={headerStyle}>
			<span>Navbar</span>
		</Header>
	)
}

export { Navbar };
