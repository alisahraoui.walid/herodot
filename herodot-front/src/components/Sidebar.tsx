import { Flex, Layout } from 'antd';
import React, { useState } from 'react';
import { OpenaiMessageDTO } from '../dto/OpenaiMessageDTO.ts';
import { OpenAiIcon } from '../icons/openai.tsx';
import { Typography } from 'antd';
import { RequestExampleWrapper } from './ChatRequestExampleWrapper.tsx';
import { TextBox } from './TextBox.tsx';

const { Text } = Typography;
const { Sider } = Layout;

const siderStyle: React.CSSProperties = {
  textAlign: 'left',
  color: '#fafafa',
  backgroundColor: '#323232',
  padding: '2em'
};

const siderTitleStyle: React.CSSProperties = {
  color: '#fafafa',
  textTransform: 'uppercase',
  fontSize: '1.2em',
  fontWeight: 'bold',
}

const conversationWrapperStyle: React.CSSProperties = {
  overflow: 'auto',
  maxHeight: '100%',
  display: 'flex',
  flexDirection: 'column',
}

const aiMessageStyle: React.CSSProperties = {
  textAlign: 'left',
  padding: '1.2em',
  fontSize: '1.2em',
}

const userMessageStyle: React.CSSProperties = {
  textAlign: 'right',
  padding: '1.2em',
  fontSize: '1.2em',
}

const Sidebar = () => {
  const [messages, setMessages] = useState<OpenaiMessageDTO[]>([]);

  return (
    <Sider width="20%" style={siderStyle}>
      <Flex vertical={true} gap={30} style={{ height: '100%' }}>
        <Flex align={'center'} gap={15}>
          <OpenAiIcon width={'20'} height={'20'} fill={'#fafafa'}/>
          <Text strong={true} style={siderTitleStyle}> More Results</Text>
        </Flex>

        {
          messages.length === 0 ?
            <Flex align={'start'} gap={5} style={{ height: '100%' }}>
              <RequestExampleWrapper setMessages={setMessages} messages={messages} />
            </Flex> :
            <></>
        }

        <div style={conversationWrapperStyle}>
          { messages.map(message => {
            const isUser = message.sender === 'user';

            return (
              <span style={ message.sender === 'user' ? userMessageStyle: aiMessageStyle }>
                { `${isUser ? 'You': 'Bot'}: ${message.message}` }
              </span>
            )
          })}
        </div>
        <TextBox setMessages={setMessages} messages={messages} />
      </Flex>
    </Sider>
  )
}

export { Sidebar };
