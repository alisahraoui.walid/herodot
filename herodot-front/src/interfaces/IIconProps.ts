interface IIconProps {
	width: string
	height: string
	fill: string
}

export type { IIconProps };
