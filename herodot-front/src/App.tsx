import React from 'react';
import MainLayout from './mainLayout.tsx';

const App: React.FC = () => (
	<MainLayout>
		<h1>Dashboard</h1>
	</MainLayout>
);

export default App;
