interface OpenaiMessageDTO {
  conversationId: string;
  sender: 'user'|'ai'
  message: string;
}

export type { OpenaiMessageDTO };
