import { useState, useCallback } from 'react';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
type FetchState = {
  loading: boolean;
  error: Error | null;
};

type UseFetchReturn<T> = FetchState & {
  fetchData: (options?: RequestInit) => Promise<T | undefined>;
};

const defaultFetchOptions: RequestInit = {
  mode: 'cors',
  headers: {
    'Content-Type': 'application/json',
  }
}

function useFetch<T>(url: string): UseFetchReturn<T> {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error | null>(null);
  const fetchData = useCallback(async (options?: RequestInit) => {
    const mergedOptions: RequestInit = { ...defaultFetchOptions, ...options };

    setLoading(true);
    setError(null);

    try {
      const response = await fetch(url, mergedOptions);
      if (!response.ok) {
        throw new Error(`Error: ${response.statusText}`);
      }
      const data = await response.json();
      return data as T;
    } catch (error) {
      setError(error as Error);
    } finally {
      setLoading(false);
    }
  }, [url]);

  return { loading, error, fetchData };
}

export default useFetch;
