import React, { ReactNode } from 'react';
import { Flex, Layout } from 'antd';
import { Navbar } from './components/Navbar.tsx';
import { Sidebar } from './components/Sidebar.tsx';

const { Content } = Layout;

interface Props {
	children: ReactNode;
}

const contentStyle: React.CSSProperties = {
	textAlign: 'center',
	lineHeight: '120px',
	color: '#323232',
	backgroundColor: '#fafafa',
};

const layoutStyle: React.CSSProperties = {
	width: '100dvw',
	height: '100dvh',
};

const MainLayout = ({ children }: Props) => {
	return (
		<Flex gap="middle" wrap>
			<Layout style={layoutStyle}>
				<Navbar />
				<Layout>
					<Sidebar />
					<Content style={contentStyle}>
						{ children }
					</Content>
				</Layout>
			</Layout>
		</Flex>
	);
}

export default MainLayout;
