from os import environ
from pydantic_settings import BaseSettings

class Settings(BaseSettings):
    api_version: str
    app_name: str
    debug_mode: bool

    class Config:
        env_file = '.env.local'
