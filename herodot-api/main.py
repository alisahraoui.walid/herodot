from fastapi import FastAPI
from config import Settings
from routers import chatbot
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI(
    title="herodot-technical-test",
)

settings = Settings()

origins = [
    'http://localhost',
    'http://localhost:5173',
    'http://localhost:5174',
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(chatbot.router)
