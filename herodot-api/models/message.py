from pydantic import BaseModel

class Message(BaseModel):
    message: str
    conversationId: str
    sender: str
