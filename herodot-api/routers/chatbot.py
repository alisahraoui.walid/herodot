from fastapi import APIRouter, Form
from models.message import Message

router = APIRouter(
    prefix='/chatbot',
    tags=['chatbot'],
    responses={
        404: {'description': 'Not found'}
    }
)

@router.post('/', status_code=200)
async def message(message: Message ):
    return Message(
        message=f'hello, your message is {message.message}',
        sender='ai',
        conversationId='1'
    )
